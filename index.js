const { hash } = window.location;
const message = atob(hash.replace('#', ''));

if(message){
    document.querySelector('#message-form').classList.add('hide');
    document.querySelector('#message-show').classList.remove('hide');

    document.querySelector('h1').innerHTML = message;
}

document.querySelector('form').addEventListener('submit', event => {
    event.preventDefault();

    document.querySelector('#message-form').classList.add('hide');
    document.querySelector('#link-form').classList.remove('hide');

    const input = document.querySelector('#message-input');
    const encrypted = btoa(input.value);

    const linkInput = document.querySelector('#link-input')
    linkInput.value = `${window.location}#${encrypted}`;
    linkInput.select();

});


//ASCII character Codes: The characters "a-z", "A-Z", "0-9", "!@#$%^&*()" and a few others can be represented with a decimal value 0 to 127 
//Base64 character codes: The characters "a-z", "A-Z", "0-9" can be represented with a decimal value from 0 to 63 
//deploy it using a free service called now: "npx now"
